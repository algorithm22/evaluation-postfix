import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  var listPos = [];
  var listIn = [];
  var listOp = [];
  var eva = [];

  int check(String a) {
    switch (a) {
      case '+':
      case '-':
        return 1;
      case '*':
      case '/':
        return 2;
      case '^':
        return 3;
    }
    return 1;
  }

// Infix
  print("Input mathematical expression");
  String? x = stdin.readLineSync()!;
  x = x.replaceAll(" ", "");
  listIn = x.split("");
  print("Infix :");
  for (int i = 0; i < listIn.length; i++) {
    stdout.write(listIn[i]);
    stdout.write(" ");
  }
  print(" ");

  // Postfix
  for (int j = 0; j < listIn.length; j++) {
    if (double.tryParse(listIn[j]) != null) {
      listPos.add(listIn[j]);
    }
    if (listIn[j] == "+" ||
        listIn[j] == "-" ||
        listIn[j] == "*" ||
        listIn[j] == "/" ||
        listIn[j] == "^") {
      while (listOp.isNotEmpty &&
          listOp[listOp.length - 1] != "(" &&
          check(listIn[j]) < check(listOp[listOp.length - 1])) {
        listPos.add(listOp[listOp.length - 1]);
        listOp.removeAt(listOp.length - 1);
      }
      listOp.add(listIn[j]);
    }
    if (listIn[j] == "(") {
      listOp.add(listIn[j]);
    }
    if (listIn[j] == ")") {
      while (listOp[listOp.length - 1] != "(") {
        listPos.add(listOp[listOp.length - 1]);
        listOp.removeAt(listOp.length - 1);
      }
      listOp.removeAt(listOp.length - 1);
    }
  }
  while (listOp.isNotEmpty) {
    listPos.add(listOp[listOp.length - 1]);
    listOp.removeAt(listOp.length - 1);
  }
  print("PostFix :");
  for (int i = 0; i < listPos.length; i++) {
    stdout.write(listPos[i]);
    stdout.write(" ");
  }
  print(" ");

// Evaluation Postfix
  for (int i = 0; i < listPos.length; i++) {
    if (double.tryParse(listPos[i]) != null) {
      eva.add(listPos[i]);
    } else {
      var l = eva[0];
      var r = eva[1];
      var left = int.parse(l);
      var right = int.parse(r);
      eva.remove(l);
      eva.remove(r);
      switch (listPos[i]) {
        case '+':
          eva.add((right + left).toString());
          break;
        case '-':
          eva.add((right - left).toString());
          break;

        case '*':
          eva.add((right * left).toString());
          break;

        case '/':
          eva.add((right / left).toString());
          break;

        case '^':
          eva.add((pow(right, left)).toString());
          break;
      }
    }
  }
  print("Evalution Postfix :");
  print(eva[0]);
}
